package com.sda.jsp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PersonDatabase {
    private static PersonDatabase ourInstance = new PersonDatabase();

    public static PersonDatabase getInstance() {
        return ourInstance;
    }

    // dodajemy w bazie "mapę person" jako pole
    private Map<Long, Person> personMap = new HashMap<>();

    // dodajemy licznik który będzie się automatycznie inkrementował
    private Long idCounter = 0L;

    private PersonDatabase() {
    }

    public void addPerson(Person person) {
        person.setId(idCounter++);
        personMap.put(person.getId(), person);
    }

    public List<Person> getPersonList(){
        return new ArrayList<>(personMap.values());
    }
}
