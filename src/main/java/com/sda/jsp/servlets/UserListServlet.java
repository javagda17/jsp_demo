package com.sda.jsp.servlets;


import com.sda.jsp.PersonDatabase;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/personList")
public class UserListServlet extends HttpServlet {
    // 1. spraw aby klasa była servletem
    // 2. dodaj w niej metodę przetwarzającą rządanie GET
    // 3. w metodzie GET zwróć widok 'listaOsob.jsp'

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // do rządania wysyłamy listę 'personów' z singletona PersonDatabase
        req.setAttribute("personListAttribute", PersonDatabase.getInstance().getPersonList());

        RequestDispatcher dispatcher = req.getRequestDispatcher("listaOsob.jsp");
        dispatcher.forward(req, resp);
    }
}
