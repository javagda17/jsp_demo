package com.sda.jsp.servlets;

import com.sda.jsp.Person;
import com.sda.jsp.PersonDatabase;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/personAdd")
public class UserAddServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String peselId = req.getParameter("peselId");

        System.out.println(firstName + " " + lastName + " " + peselId);

        // 1. stwórz obiekt Person,
        Person person = new Person();
        person.setName(firstName);
        person.setSurname(lastName);
        person.setPESEL(peselId);

        // 2. dodaj go do bazy (PersonDatabase)
        PersonDatabase.getInstance().addPerson(person);
        
        // 3. wykonać przekierowanie na adres  /personList
        resp.sendRedirect("/personList"); // <--
    }
}
