<%@ page import="com.sda.jsp.Person" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 12/6/18
  Time: 8:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Lista Osób</title>
</head>
<body>
<a href="wyswietlaczTabliczkiMnozenia.jsp">Tabliczka Mnozenia</a>
<a href="wyswietlaczTabliczkiMnozenia2.jsp">Tabliczka Mnozenia 2</a>
<a href="/personList">Lista Osob</a>
<a href="index.jsp">Index</a>
</br>

<table>
    <%
        List<Person> personList = (List<Person>) request.getAttribute("personListAttribute");

        for (Person person : personList) {
            out.print("<tr>");
            out.print("<td>" + person.getId() + "</td>");
            out.print("<td>" + person.getName() + "</td>");
            out.print("<td>" + person.getSurname() + "</td>");
            out.print("<td>" + person.getPESEL() + "</td>");
            out.print("</tr>");
        }
    %>
</table>
</body>
</html>
