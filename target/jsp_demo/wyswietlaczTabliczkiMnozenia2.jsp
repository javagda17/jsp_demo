<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 12/6/18
  Time: 8:07 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<a href="wyswietlaczTabliczkiMnozenia.jsp">Tabliczka Mnozenia</a>
<a href="wyswietlaczTabliczkiMnozenia2.jsp">Tabliczka Mnozenia 2</a>
<a href="listaOsob.jsp">Lista Osob</a>
<a href="index.jsp">Index</a>
</br>

<form action="wyswietlaczTabliczkiMnozenia2.jsp" method="get">
    <input type="number" name="rozmiarX" id="rozmiarX">
    <input type="number" name="rozmiarY" id="rozmiarY">
    <input type="submit" value="Pokaż">
</form>

<hr>
<%
    String rozmiarx = request.getParameter("rozmiarX");
    String rozmiary = request.getParameter("rozmiarY");
    if (rozmiarx != null && rozmiary != null && !rozmiarx.isEmpty() && !rozmiary.isEmpty()) {
        Integer rozmiarIntX = Integer.parseInt(rozmiarx);
        Integer rozmiarIntY = Integer.parseInt(rozmiary);

        out.print("<table>");
        for (int i = 0; i < rozmiarIntX; i++) { // wiersze
            out.print("<tr>");
            for (int j = 0; j < rozmiarIntY; j++) { // kolumny
                out.print("<td style=\"border: 1px solid #000000\" >");
                out.print((i + 1) * (j + 1));
                out.print("</td>");
            }
            out.print("</tr>");
        }
        out.print("</table>");
    } else {
        out.print("Nie podano parametru!");
    }
%>

</body>
</html>
