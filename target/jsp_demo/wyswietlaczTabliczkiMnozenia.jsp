<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 12/6/18
  Time: 7:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Wyswietlacz Tabliczki Mnozenia</title>
</head>
<body>
<a href="wyswietlaczTabliczkiMnozenia.jsp">Tabliczka Mnozenia</a>
<a href="wyswietlaczTabliczkiMnozenia2.jsp">Tabliczka Mnozenia 2</a>
<a href="listaOsob.jsp">Lista Osob</a>
<a href="index.jsp">Index</a>
</br>

<form action="wyswietlaczTabliczkiMnozenia.jsp" method="post">
    <input type="number" name="rozmiar" id="rozmiar">
    <input type="submit" value="Pokaż">
</form>

<hr>
<%
    String rozmiar = request.getParameter("rozmiar");
    if (rozmiar != null && !rozmiar.isEmpty()) {
        Integer rozmiarInt = Integer.parseInt(rozmiar);

        out.print("<table>");
        for (int i = 0; i < rozmiarInt; i++) { // wiersze
            out.print("<tr>");
            for (int j = 0; j < rozmiarInt; j++) { // kolumny
                out.print("<td style=\"border: 1px solid #000000\" >");
                out.print((i + 1) * (j + 1));
                out.print("</td>");
            }
            out.print("</tr>");
        }
        out.print("</table>");
    } else {
        out.print("Nie podano parametru!");
    }
%>

</body>
</html>
